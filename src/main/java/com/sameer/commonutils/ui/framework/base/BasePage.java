package com.sameer.commonutils.ui.framework.base;

/**
 * This is Base class for all PageObject classes. All PageObject classes should
 * be extend this class.
 */
public abstract class BasePage extends Base {

	/**
	 * This is a generic method that takes a pageInstance parameter, which is a
	 * Class object representing a page class that extends BasePage. It's expected
	 * to return an instance of the specified page class.
	 * 
	 * @param <TPage>      - Generic Class
	 * @param pageInstance - Page class object.
	 * @return - Current instance as an instance of the specified page class.
	 */
	@SuppressWarnings("unchecked")
	public <TPage extends BasePage> TPage As(Class<TPage> pageInstance) {
		try {
			return (TPage) this;
		} catch (Exception e) {
			e.getStackTrace();
		}

		return null;
	}

}
