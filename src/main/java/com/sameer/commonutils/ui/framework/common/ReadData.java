package com.sameer.commonutils.ui.framework.common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * This class contain method related to getting data from different files like
 * excel, csv, txt file.
 */
public class ReadData {

	private static final Logger log = LogManager.getLogger(ReadData.class);

	public static String[][] fromExcel(String filePath, String sheetName) {

		try {
			FileInputStream fileStream = new FileInputStream(new File(filePath));

			try (XSSFWorkbook workbook = new XSSFWorkbook(fileStream)) {
				XSSFSheet sheet = workbook.getSheet(sheetName);

				Iterator<Row> iterator = sheet.iterator();

				String[][] data = new String[sheet.getLastRowNum() + 1][sheet.getRow(0).getLastCellNum()];

				int r = 0;
				int c = 0;
				while (iterator.hasNext()) {
					Row row = iterator.next();

					Iterator<Cell> cellIterator = row.iterator();
					c = 0;
					while (cellIterator.hasNext()) {
						Cell cell = cellIterator.next();

						switch (cell.getCellType()) {
						case NUMERIC:
							data[r][c] = String.valueOf(cell.getNumericCellValue());
							break;

						case STRING:
							data[r][c] = cell.getStringCellValue();
							break;

						default:
							break;

						}
						c++;
					}
					r++;
				}

				return data;
			}

		} catch (Exception e) {
			log.error("Error in getting data from excel file.", e);
		}

		return null;
	}

	public static String[] rowFromExcel(String filePath, String sheetName, int rowIndex) {

		try {
			FileInputStream fileStream = new FileInputStream(new File(filePath));

			try (XSSFWorkbook workbook = new XSSFWorkbook(fileStream)) {
				XSSFSheet sheet = workbook.getSheet(sheetName);

				String[] data = new String[sheet.getRow(0).getLastCellNum()];

				Row row = sheet.getRow(rowIndex);
				Iterator<Cell> cellIterator = row.iterator();

				int c = 0;
				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();

					switch (cell.getCellType()) {
					case NUMERIC:
						data[c] = String.valueOf(cell.getNumericCellValue());
						break;

					case STRING:
						data[c] = cell.getStringCellValue();
						break;

					default:
						break;
					}
					c++;
				}

				return data;
			}

		} catch (Exception e) {
			log.error("Error in getting data from excel file.", e);
		}

		return null;
	}

	public static String[] columnfromExcel(String filePath, String sheetName, int columnIndex) {

		try {
			FileInputStream fileStream = new FileInputStream(new File(filePath));

			try (XSSFWorkbook workbook = new XSSFWorkbook(fileStream)) {
				XSSFSheet sheet = workbook.getSheet(sheetName);

				Iterator<Row> iterator = sheet.iterator();

				String[] data = new String[sheet.getLastRowNum() + 1];

				int r = 0;
				while (iterator.hasNext()) {
					Row row = iterator.next();

					Cell cell = row.getCell(columnIndex);

					switch (cell.getCellType()) {
					case NUMERIC:
						data[r] = String.valueOf(cell.getNumericCellValue());
						break;

					case STRING:
						data[r] = cell.getStringCellValue();
						break;

					default:
						break;

					}
					r++;
				}

				return data;
			}

		} catch (Exception e) {
			log.error("Error in getting data from excel file.", e);
		}

		return null;
	}

	public static String[][] fromCSV(String filePath) {
		List<String[]> resultsList = new ArrayList<String[]>();
		try {
			BufferedReader reader = new BufferedReader(new FileReader(filePath));
			String line;
			while ((line = reader.readLine()) != null) {
				String[] temp = line.split(",");
				resultsList.add(temp);
			}
			reader.close();
		} catch (Exception e) {
			log.error("Error in getting data from csv file.", e);
		}

		String[][] resultsArray = new String[resultsList.size()][resultsList.get(0).length];

		for (int i = 0; i < resultsList.size(); i++) {
			for (int j = 0; j < resultsList.get(0).length; j++) {
				resultsArray[i][j] = resultsList.get(i)[j];
			}
		}

		return resultsArray;
	}

	public static List<String> fromTextFile(String filePath) {
		List<String> data = new ArrayList<String>();
		try {
			FileReader file = new FileReader(filePath);
			BufferedReader bfr = new BufferedReader(file);
			String line;
			while ((line = bfr.readLine()) != null) {
				data.add(line);
			}

			bfr.close();
			file.close();
		} catch (Exception e) {
			log.error("Error in getting data from txt file.", e);
		}

		return data;
	}

	public static String fromTextFile_(String filePath) {
		String data = "";
		try {
			FileReader file = new FileReader(filePath);
			BufferedReader bfr = new BufferedReader(file);
			String line;
			while ((line = bfr.readLine()) != null) {
				data = data + line;
			}

			bfr.close();
			file.close();
		} catch (Exception e) {
			log.error("Error in getting data from txt file.", e);
		}

		return data;
	}

}
