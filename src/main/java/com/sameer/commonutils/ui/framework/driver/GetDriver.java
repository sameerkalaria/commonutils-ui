package com.sameer.commonutils.ui.framework.driver;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;

import com.sameer.commonutils.ui.framework.config.Settings;

import io.github.bonigarcia.wdm.WebDriverManager;

/**
 * This class contains method for getting WebDriver context.
 */
public class GetDriver {
	private static WebDriver driver = null;
	private static final Logger log = LogManager.getLogger(GetDriver.class);

	/**
	 * Return WebDriver instance of passed BrowserType. For ex. Firefox, Chrome
	 * 
	 * @param browser - BrowserType.
	 * @return - Instance of WebDriver with specified browser type.
	 */
	public static WebDriver getDriver(BrowserType browser) {
		switch (browser) {
		case CHROME:

			try {
				// Don't required as in Selenium4 we can use Selenium Manager
				// WebDriverManager.chromedriver().setup();

				if (Settings.remote) {
					ChromeOptions options = new ChromeOptions();
					options.addArguments("--start-maximized");
					options.addArguments("--disable-infobars");
					driver = new RemoteWebDriver(new URL(Settings.hubURL), options);
				} else {
					driver = new ChromeDriver();
				}
			} catch (UnreachableBrowserException e) {
				log.error("Exception while launching browser.", e);
			} catch (WebDriverException e) {
				log.error("Exception while launching browser.", e);
			} catch (MalformedURLException e) {
				log.error("Exception while launching browser.", e);
			}

			break;

		case FIREFOX:

			try {
				// Don't required as in Selenium4 we can use Selenium Manager
				// WebDriverManager.firefoxdriver().setup();

				if (Settings.remote) {
					FirefoxOptions options = new FirefoxOptions();
					options.addArguments("--start-maximized");
					options.addArguments("--disable-infobars");
					driver = new RemoteWebDriver(new URL(Settings.hubURL), options);
				} else {
					driver = new FirefoxDriver();
				}
			} catch (UnreachableBrowserException e) {
				log.error("Exception while launching browser.", e);
			} catch (WebDriverException e) {
				log.error("Exception while launching browser.", e);
			} catch (MalformedURLException e) {
				log.error("Exception while launching browser.", e);
			}

			break;

		case SAFARI:

			try {
				// It is required as Selenium Manager only supports chrome, firefox and edge
				// browser
				WebDriverManager.safaridriver().setup();

				if (Settings.remote) {
					SafariOptions options = new SafariOptions();
					options.setCapability("--start-maximized", true);
					driver = new RemoteWebDriver(new URL(Settings.hubURL), options);
				} else {
					driver = new SafariDriver();
				}
			} catch (UnreachableBrowserException e) {
				log.error("Exception while launching browser.", e);
			} catch (WebDriverException e) {
				log.error("Exception while launching browser.", e);
			} catch (MalformedURLException e) {
				log.error("Exception while launching browser.", e);
			}
		}

		return driver;
	}

	/**
	 * Return WebDriver instance of passed browsername as String. For ex. Firefox,
	 * Chrome
	 * 
	 * @param browserName - String. Name of browser. Chrome, Firefox
	 * @return - Instance of WebDriver with specified browser type.
	 */
	public static WebDriver getDriver(String browserName) {
		if (browserName.equalsIgnoreCase("chrome")) {
			driver = getDriver(BrowserType.CHROME);
		} else if (browserName.equalsIgnoreCase("firefox")) {
			driver = getDriver(BrowserType.FIREFOX);
		} else if (browserName.equalsIgnoreCase("safari")) {
			driver = getDriver(BrowserType.SAFARI);
		}

		return driver;
	}

}
