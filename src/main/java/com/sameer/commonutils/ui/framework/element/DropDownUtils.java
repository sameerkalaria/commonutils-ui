package com.sameer.commonutils.ui.framework.element;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

/**
 * This class contains methods related t dropdown element
 */
public class DropDownUtils {

	private static final Logger log = LogManager.getLogger(DropDownUtils.class);

	/**
	 * It will select option from dropdown using text passed as parameter and assert
	 * that correct option is selected otherwise throws exception.
	 * 
	 * @param dropdown - WebElement. Dropdown webelement.
	 * @param text     - String. Text of option which want to select in dropdown.
	 */
	public static void selecOptionUsingText(WebElement dropdown, String text) {
		ElementUtils.waitTillElementVisible(dropdown);
		Select select = new Select(dropdown);
		select.selectByVisibleText(text);

		WebElement option = select.getFirstSelectedOption();
		Assert.assertTrue(ElementUtils.getText(option).equals(text), "Option is not selected.");
		log.info(text + " option selected.");
	}

	/**
	 * It will select option from dropdown using index passed as parameter and
	 * assert that correct option is selected otherwise throws exception.
	 * 
	 * @param dropdown - WebElement. Dropdown webelement.
	 * @param index    - Integer. Index of option which want to select in dropdown.
	 */
	public static void selecOptionUsingIndex(WebElement dropdown, int index) {
		ElementUtils.waitTillElementVisible(dropdown);
		Select select = new Select(dropdown);
		select.selectByIndex(index);
	}

	/**
	 * It will select option from dropdown using value passed as parameter and
	 * assert that correct option is selected otherwise throws exception.
	 * 
	 * @param dropdown - WebElement. Dropdown webelement.
	 * @param value    - String. Value of option which want to select in dropdown.
	 */
	public static void selecOptionUsingValue(WebElement dropdown, String value) {
		ElementUtils.waitTillElementVisible(dropdown);
		Select select = new Select(dropdown);
		select.selectByValue(value);
	}

	/**
	 * Return list of all options of dropdown.
	 * 
	 * @param dropdown - WebElement. Dropdown webelement.
	 * @return - List of all options.
	 */
	public static List<String> getAllOptions(WebElement dropdown) {
		ElementUtils.waitTillElementVisible(dropdown);
		Select select = new Select(dropdown);

		List<String> options = new ArrayList<String>();
		for (WebElement ele : select.getOptions()) {
			options.add(ElementUtils.getText(ele));
		}

		return options;
	}

	/**
	 * Return list of all selected options of dropdown.
	 * 
	 * @param dropdown - WebElement. Dropdown webelement.
	 * @return - List of all selected options.
	 */
	public static List<String> getAllSelectedOptions(WebElement dropdown) {
		ElementUtils.waitTillElementVisible(dropdown);
		Select select = new Select(dropdown);

		List<String> options = new ArrayList<String>();
		for (WebElement ele : select.getAllSelectedOptions()) {
			options.add(ElementUtils.getText(ele));
		}

		return options;
	}

	/**
	 * Select any random option from dropdown
	 * 
	 * @param dropdown - WebElement. Dropdown webelement.
	 */
	public static void selectRandonOption(WebElement dropdown) {

		ElementUtils.waitTillElementVisible(dropdown);
		Select select = new Select(dropdown);
		int totalOptions = select.getAllSelectedOptions().size();

		Random random = new Random();
		int randomIndex = random.nextInt(totalOptions);
		select.selectByIndex(randomIndex);

	}

	/**
	 * Deselect option from dropdown.
	 * 
	 * @param dropdown - WebElement. Dropdown webelement.
	 * @param option   - String. Text of option which want to deselect from
	 *                 dropdown.
	 */
	public static void deselectOption(WebElement dropdown, String option) {

		ElementUtils.waitTillElementVisible(dropdown);
		Select select = new Select(dropdown);
		select.deselectByVisibleText(option);
	}

	/**
	 * Deselect all option from dropdown.
	 * 
	 * @param dropdown - WebElement. Dropdown webelement.
	 */
	public static void deselectAllOption(WebElement dropdown) {

		ElementUtils.waitTillElementVisible(dropdown);
		Select select = new Select(dropdown);
		select.deselectAll();
	}

}
