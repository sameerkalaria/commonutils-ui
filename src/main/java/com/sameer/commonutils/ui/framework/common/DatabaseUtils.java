package com.sameer.commonutils.ui.framework.common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.aventstack.extentreports.Status;
import com.sameer.commonutils.ui.framework.config.Settings;

/**
 * This class contains methods related to database operations.
 */
public class DatabaseUtils {
	private static final Logger log = LogManager.getLogger(DatabaseUtils.class);
	private static Connection connection;

	/**
	 * Setting connection object in ThreadLocal in DatabaseConnection class.
	 */
	public static void getConnection() {
		try {
			if (connection == null || connection.isClosed()) {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				connection = DriverManager.getConnection(Settings.connectionString);
				log.info("Database connection successful.");
				ReportUtils.addLog(Status.PASS, "Database connection successful.");
			}
		} catch (Exception e) {
			log.error("Error in connect with database.", e);
			ReportUtils.addLog(Status.FAIL, e);
		}

		DatabaseConnection.setConnection(connection);
	}

	public static ResultSet executeQuery(String query) {
		ResultSet result = null;
		try {
			Statement statement = DatabaseConnection.getConnection().createStatement();
			result = statement.executeQuery(query);
			log.info("Query executed successful.");
			ReportUtils.addLog(Status.PASS, "Query executed successful.");
		} catch (SQLException e) {
			log.error("Error in execute query", e);
			ReportUtils.addLog(Status.FAIL, e);
		}

		return result;
	}

	public static String[][] getDataFromResultSet(ResultSet result) throws SQLException {
		ResultSetMetaData rsmd = result.getMetaData();

		int column = rsmd.getColumnCount();
		result.last();
		int row = result.getRow();
		result.first();

		String[][] data = new String[row][column];

		// Convert result set into String array
		int i = 0;
		while (result.next()) {
			for (int j = 0; j < column; j++) {
				data[i][j] = result.getString(j + 1);
			}
			i++;
		}

		return data;
	}

	public static int executeUpdateQuery(String query) {
		int result = 0;
		try {
			Statement statement = DatabaseConnection.getConnection().createStatement();
			result = statement.executeUpdate(query);

			log.info("Update query executed successful. " + result + " records updated.");
			ReportUtils.addLog(Status.PASS, "Update query executed successful. " + result + " records updated.");
		} catch (SQLException e) {
			log.error("Error in execute update query", e);
			ReportUtils.addLog(Status.FAIL, e);
		}

		return result;
	}

}
