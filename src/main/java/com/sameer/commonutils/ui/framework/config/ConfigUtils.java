package com.sameer.commonutils.ui.framework.config;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sameer.commonutils.ui.framework.common.GeneralUtils;
import com.sameer.commonutils.ui.framework.driver.BrowserType;

/**
 * This class is for getting data from properties file.
 */
public class ConfigUtils {
	private static Properties config = new Properties();
	private static final String PROPERTY_FILEPATH = GeneralUtils
			.normalizedFilePath("/src/main/resources/config.properties");
	private static final Logger log = LogManager.getLogger(ConfigUtils.class);

	/**
	 * Get instance of Properties class with load properties file. Property file
	 * name and location must be "/src/main/resources/config.properties"
	 * 
	 * @return - Instance of Properties class
	 * @throws IOException
	 */
	public static Properties getConfigInstance() throws IOException {
		BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(PROPERTY_FILEPATH));
			config = new Properties();
			try {
				config.load(reader);
				reader.close();
			} catch (IOException e) {
				log.info("Error while reading config file.", e);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException("config.properties not found at " + PROPERTY_FILEPATH);
		}

		return config;
	}

	/**
	 * It sets all properties value from configuration property file into static
	 * member of Settings class. Property file name and location must be
	 * "/src/main/resources/config.properties"
	 * 
	 * @throws IOException
	 */
	public static void getSettings() throws IOException {
		getConfigInstance();

		Settings.OS = getProperty("os");
		Settings.appURL = getProperty("appURL");
		Settings.username = getProperty("username");
		Settings.password = getProperty("password");
		Settings.browserType = BrowserType.valueOf(getProperty("browserType", "firefox").toUpperCase());
		Settings.remote = Boolean.parseBoolean(getProperty("remote", "false"));
		Settings.explicitWait = Integer.parseInt(getProperty("explicitWait"));
		Settings.timeStampFormat = getProperty("timeStampFormat");

		Settings.hubURL = getProperty("hubURL");

		// set extent report setting for html report
		Settings.documentName = getProperty("documentName");
		Settings.reportName = getProperty("reportName");
		Settings.timeStamp = getProperty("timeStamp");

		Settings.theme = getProperty("theme", "standard");
		Settings.pathToSave = getProperty("pathToSave");
		Settings.htmlReportName = getProperty("htmlReportName");
		Settings.htmlReportName = Settings.htmlReportName.endsWith(".html") ? Settings.htmlReportName
				: Settings.htmlReportName + ".html";

		// database settings
		Settings.connectionString = getProperty("connectionString");
	}

	private static String getProperty(String propertyName) {

		String systemProperty = System.getProperty(propertyName);
		String value = systemProperty == null ? config.getProperty(propertyName, null) : systemProperty;

		return value;
	}

	private static String getProperty(String propertyName, String defaultValue) {

		String systemProperty = System.getProperty(propertyName);
		String value = systemProperty == null ? config.getProperty(propertyName, defaultValue) : systemProperty;

		return value;
	}

}
