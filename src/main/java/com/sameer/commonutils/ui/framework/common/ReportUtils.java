package com.sameer.commonutils.ui.framework.common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.model.Media;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.sameer.commonutils.ui.framework.config.Settings;

/**
 * This class is for Extent report related functionalities like create report,
 * add logs etc.
 */
public class ReportUtils {
	private static final Logger log = LogManager.getLogger(ReportUtils.class);
	public static ExtentReports report;

	/**
	 * Get Object of HTMl Reporter in Extent Report.
	 * 
	 * @return - Object of ExtentSparkReporter.
	 */
	private static ExtentSparkReporter getHTMLReporter() {
		ExtentSparkReporter spark = new ExtentSparkReporter(
				GeneralUtils.normalizedFilePath(Settings.pathToSave) + Settings.htmlReportName);
		spark.config().setDocumentTitle(Settings.documentName);
		spark.config().setReportName(Settings.reportName);
		spark.config().setTimeStampFormat(Settings.timeStamp);

		if (Settings.theme.equalsIgnoreCase("dark")) {
			spark.config().setTheme(Theme.DARK);
		} else {
			spark.config().setTheme(Theme.STANDARD);
		}

		return spark;

	}

	/**
	 * It will create instance of ExtentReports class and attached HTML reporter
	 * from method getHTMLReporter.
	 */
	public static void createReport() {
		report = new ExtentReports();
		report.attachReporter(getHTMLReporter());

		log.info("Extent report is created with HTML configuration");
	}

	/**
	 * Create instance of ExtentTest and add into Extent report.
	 * 
	 * @param testName - Strung. Name of the test you want into extent report.
	 */
	public static void createTest(String testName) {
		ExtentTest test = report.createTest(testName);
		LocalExtentTestContext.setExtentTest(test);

		log.info("Extent test is created with name " + testName);
	}

	/**
	 * Add log into extent test with message.
	 * 
	 * @param status - Status Enum. Like PASS, FAIL, SKIP
	 * @param msg    - String. Log which you want to add into report for specific
	 *               step.
	 */
	public static void addLog(Status status, String msg) {
		LocalExtentTestContext.getExtentTest().log(status, msg);
	}

	/**
	 * Add log into extent test with Throwable error.
	 * 
	 * @param status - Status Enum. Like PASS, FAIL, SKIP
	 * @param t      - Throwable. Throwable error which you want to add into report
	 *               for specific step.
	 */
	public static void addLog(Status status, Throwable t) {
		LocalExtentTestContext.getExtentTest().log(status, t);
	}

	/**
	 * Add log into extent test with message.
	 * 
	 * @param status - Status Enum. Like PASS, FAIL, SKIP
	 * @param msg    - Markup. Any HTML markup which you want to add into report for
	 *               specific step.
	 */
	public static void addLog(Status status, Markup markup) {
		LocalExtentTestContext.getExtentTest().log(status, markup);
	}

	public static void addLog(Status status, Media media) {
		LocalExtentTestContext.getExtentTest().log(status, media);
	}

	/**
	 * Add log into extent test with message.
	 * 
	 * @param status - Status Enum. Like PASS, FAIL, SKIP
	 * @param msg    - Media. Any media file which you want to add into report for
	 *               specific step.
	 */
	public static void addScrrenshot(Status status) {
		String name = "screenshot_" + GeneralUtils.getCurrentTimeStamp();
		String screenshotPath = TakeScreenshot.take(name, "/test-output/");

		LocalExtentTestContext.getExtentTest().log(status, "Screenshot below : "
				+ LocalExtentTestContext.getExtentTest().addScreenCaptureFromPath(screenshotPath));
	}

}
