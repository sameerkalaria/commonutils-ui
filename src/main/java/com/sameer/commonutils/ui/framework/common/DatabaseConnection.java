package com.sameer.commonutils.ui.framework.common;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * This class is for handle Database Connection context.IT is thread safe as it
 * uses ThreaLocal for storing database connection context.
 */
public class DatabaseConnection {
	private static ThreadLocal<Connection> connection = new ThreadLocal<>();

	public static Connection getConnection() throws SQLException {
		if ((connection.get() == null || connection.get().isClosed())) {
			DatabaseUtils.getConnection();
		}
		return connection.get();
	}

	public static void setConnection(Connection con) {
		connection.set(con);
	}

	public static void closeConection() throws SQLException {
		if (connection.get() != null) {
			connection.get().close();
		}
	}

}
