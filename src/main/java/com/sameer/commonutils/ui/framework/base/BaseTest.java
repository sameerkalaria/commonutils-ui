package com.sameer.commonutils.ui.framework.base;

import java.io.IOException;
import java.lang.reflect.Method;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.Status;
import com.sameer.commonutils.ui.framework.common.DatabaseConnection;
import com.sameer.commonutils.ui.framework.common.ReportUtils;
import com.sameer.commonutils.ui.framework.config.ConfigUtils;
import com.sameer.commonutils.ui.framework.config.Settings;
import com.sameer.commonutils.ui.framework.driver.GetDriver;

/**
 * This is Base class for all Test case classes. All Test case class should be
 * extend this class. It
 * contains @BeforeSuite, @AfterSuite, @BeforeClass, @AfterClass, @BeforeMethod, @AfterMethod
 * methods. WebDriver context, Extent Report context, configuration file reader
 * is setup in this class.
 */
public class BaseTest extends Base {
	// protected BasePage currentPage;
	protected static final Logger log = LogManager.getLogger(BaseTest.class);

	/**
	 * Getting data from property file and store it into Settings. Create extent
	 * report for suite.
	 */
	@BeforeSuite
	public void initializeConfig() throws IOException {
		log.info("Before suite started");

		ConfigUtils.getSettings();
		ReportUtils.createReport();
	}

	/**
	 * It will execute before every @Test method. Create test for that test case and
	 * add it into extent report.
	 * 
	 * @param method - Instance of Method class. It will automatically added from
	 *               calling method by TestNG.
	 */
	@BeforeMethod
	public void createTest(Method method) {
		// LocalExtentTestContext.setExtentTest(ReportUtils.createTest(method.getName()));
		ReportUtils.createTest(method.getName());
		ReportUtils.addLog(Status.PASS, "Start test");
	}

	/**
	 * It will execute after every @Test method. Add log into extent report based in
	 * test case status like PASS, FAIL or SKIP.
	 * 
	 * @param result - Instance of ITestResult interface. It will automatically
	 *               added by TestNG.
	 * @param method - Instance of Method class. It will automatically added from
	 *               calling method by TestNG.
	 */
	@AfterMethod
	public void addStatus(ITestResult result, Method method) {
		switch (result.getStatus()) {
		case ITestResult.SUCCESS:
			ReportUtils.addLog(Status.PASS, method.getName() + " testcase passed.");
			break;

		case ITestResult.FAILURE:
			ReportUtils.addLog(Status.FAIL, method.getName() + " testcase failed.");
			break;

		case ITestResult.SKIP:
			ReportUtils.addLog(Status.SKIP, method.getName() + " testcase skipped.");
			break;
		}
	}

	/**
	 * It will execute before every class start execution of test cases. It
	 * initialize WebDriver.
	 */
	@BeforeClass(alwaysRun = true)
	public void setupWebDriver() throws IOException {
		log.info("Before class started");

		setupDriver();
		// currentPage = new BasePage();

	}

	/**
	 * It will close WebDriver instance for this class. if any Database connection
	 * is active then it will close that connection.
	 * 
	 * @throws SQLException
	 */
	@AfterClass(alwaysRun = true)
	public void cleanUp() throws SQLException {
		log.info("After class started");

		// closing webdriver
		if (LocalWebDriverContext.getLocalDriver() != null) {
			LocalWebDriverContext.closeDriver();
		}

		DatabaseConnection.closeConection();

	}

	/**
	 * It will execute after all the classes in suite executed. It will flush the
	 * extent report. Close and quit all the WebDriver instances. Close Database
	 * connection.
	 */
	@AfterSuite
	public void clean() throws SQLException {
		log.info("After suite started");

		ReportUtils.report.flush();

		// Quit webdriver
		if (LocalWebDriverContext.getLocalDriver() != null) {
			LocalWebDriverContext.getLocalDriver().quit();
		}

		DatabaseConnection.closeConection();
	}

	/**
	 * It will initialize WebDriver instance of browser mentioned in properties
	 * file. Set that WebDriver context to LocalWebDriverContext.
	 */
	private void setupDriver() {
		LocalWebDriverContext.setLocalDriver(GetDriver.getDriver(Settings.browserType));
		log.info("Browser setup by Thread " + Thread.currentThread().getId() + " and Driver reference is : "
				+ LocalWebDriverContext.getLocalDriver());
	}

}
