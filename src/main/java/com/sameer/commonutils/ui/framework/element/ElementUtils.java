package com.sameer.commonutils.ui.framework.element;

import java.time.Duration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.sameer.commonutils.ui.framework.base.LocalWebDriverContext;
import com.sameer.commonutils.ui.framework.config.Settings;

/**
 * This class contains methods related to operations on WebElement.
 */
public class ElementUtils {
	private static final Logger log = LogManager.getLogger(ElementUtils.class);

	/**
	 * Wait till element is visible up to duration specified in seconds
	 * 
	 * @param element - WebElement. WebElement for which want to wait.
	 * @param seconds - Integer. Till how many seconds to wait.
	 */
	public static void waitTillElementVisible(WebElement element, int seconds) {
		WebDriverWait wait = new WebDriverWait(LocalWebDriverContext.getLocalDriver(), Duration.ofSeconds(seconds));
		wait.until(ExpectedConditions.visibilityOf(element));
	}

	/**
	 * Wait till element is visible up to duration specified in configuration file
	 * with property name 'explicitWait'
	 * 
	 * @param element - WebElement. WebElement for which want to wait.
	 */
	public static void waitTillElementVisible(WebElement element) {
		waitTillElementVisible(element, Settings.explicitWait);
	}

	/**
	 * Wait till element will be loaded in DOM up to duration specified in seconds
	 * 
	 * @param by      - By. Element using By mechanism.
	 * @param seconds - Integer. Till how many seconds to wait.
	 */
	public static void waitTillElementPresentInDOM(By by, int seconds) {
		WebDriverWait wait = new WebDriverWait(LocalWebDriverContext.getLocalDriver(), Duration.ofSeconds(seconds));
		wait.until(ExpectedConditions.presenceOfElementLocated(by));
	}

	/**
	 * Wait till element will be loaded in DOM up to duration specified in
	 * configuration file with property name 'explicitWait'
	 * 
	 * @param by - By. Element using By mechanism.
	 */
	public static void waitTillElementPresentInDOM(By by) {
		waitTillElementPresentInDOM(by, Settings.explicitWait);
	}

	/**
	 * Wait till element is clickable up to duration specified in seconds
	 * 
	 * @param element - WebElement. WebElement for which want to wait.
	 * @param seconds - Integer. Till how many seconds to wait.
	 */
	public static void waitTillElementClickable(WebElement element, int seconds) {
		WebDriverWait wait = new WebDriverWait(LocalWebDriverContext.getLocalDriver(), Duration.ofSeconds(seconds));
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	/**
	 * Wait till element is clickable up to duration specified in configuration file
	 * with property name 'explicitWait'
	 * 
	 * @param element - WebElement. WebElement for which want to wait.
	 */
	public static void waitTillElementClickable(WebElement element) {
		waitTillElementClickable(element, Settings.explicitWait);
	}

	/**
	 * Wait till element is invisible up to duration specified in seconds
	 * 
	 * @param element - WebElement. WebElement for which want to wait.
	 * @param seconds - Integer. Till how many seconds to wait.
	 */
	public static void waitTillElementInvisible(WebElement element, int seconds) {
		WebDriverWait wait = new WebDriverWait(LocalWebDriverContext.getLocalDriver(), Duration.ofSeconds(seconds));
		wait.until(ExpectedConditions.invisibilityOf(element));
	}

	/**
	 * Wait till element is invisible up to duration specified in configuration file
	 * with property name 'explicitWait'
	 * 
	 * @param element - WebElement. WebElement for which want to wait.
	 */
	public static void waitTillElementInvisible(WebElement element) {
		waitTillElementInvisible(element, Settings.explicitWait);
	}

	/**
	 * click on element. It will wait till element is clickable up to duration
	 * specified in configuration file with property name 'explicitWait'.
	 * 
	 * @param element - WebElement. WebElement for which want to click.
	 */
	public static void clickOnElement(WebElement element) {
		waitTillElementClickable(element);
		element.click();
		log.info("Clicked on element.");
	}

	/**
	 * Click on element using JavaScript. It will wait till element is clickable up
	 * to duration specified in configuration file with property name 'explicitWait'
	 * 
	 * @param element - WebElement. WebElement for which want to click.
	 */
	public static void clickOnElementUsingJS(WebElement element) {
		waitTillElementClickable(element);
		JavascriptExecutor js = (JavascriptExecutor) LocalWebDriverContext.getLocalDriver();
		js.executeScript("arguments[0].click();", element);
		log.info("Clicked on element.");
	}

	/**
	 * click on element and return true if click success otherwise false. It will
	 * wait till element is clickable up to duration specified in configuration file
	 * with property name 'explicitWait'.
	 * 
	 * @param element - WebElement. WebElement for which want to click.
	 * @return - True if click success otherwise False.
	 */
	public static Boolean clickOnElement_(WebElement element) {
		try {
			waitTillElementClickable(element);
			element.click();
			log.info("Clicked on element.");
			return true;
		} catch (Exception e) {
			log.info("Failed to click on element.");
			return false;
		}
	}

	/**
	 * click on element using JavaScript and return true if click success otherwise
	 * false. It will wait till element is clickable up to duration specified in
	 * configuration file with property name 'explicitWait'.
	 * 
	 * @param element - WebElement. WebElement for which want to click.
	 * @return - True if click success otherwise False.
	 */
	public static Boolean clickOnElementUsingJS_(WebElement element) {
		try {
			waitTillElementClickable(element);
			JavascriptExecutor js = (JavascriptExecutor) LocalWebDriverContext.getLocalDriver();
			js.executeScript("arguments[0].click();", element);
			log.info("Clicked on element.");
			return true;
		} catch (Exception e) {
			log.info("Failed to click on element.");
			return false;
		}
	}

	/**
	 * Enter text in element. It will wait till element is visible up to duration
	 * specified in configuration file with property name 'explicitWait'.
	 * 
	 * @param element - WebElement. WebElement for which want to enter text.
	 * @param text    - String. Text which want to enter.
	 */
	public static void enterText(WebElement element, String text) {
		waitTillElementVisible(element);
		element.sendKeys(text);
		log.info("Text entered in element.");
	}

	/**
	 * Get text of element. It will wait till element is visible up to duration
	 * specified in configuration file with property name 'explicitWait'.
	 * 
	 * @param element - WebElement. WebElement for which want to get text.
	 * @return - Text of specified WebElemetn.
	 */
	public static String getText(WebElement element) {
		waitTillElementVisible(element);
		return element.getText();
	}

	/**
	 * Get attribute value of element. It will wait till element is visible up to
	 * duration specified in configuration file with property name 'explicitWait'.
	 * 
	 * @param element   - WebElement. WebElement for which want to get attribute
	 *                  value.
	 * @param attribute - String. Name of attribute of WebElement which value want.
	 * @return - Value of specified attribute of WebElement
	 */
	public static String getAttributeValue(WebElement element, String attribute) {
		waitTillElementVisible(element);
		return element.getAttribute(attribute);
	}

	/**
	 * Verify element is visible or not. If visible then return true otherwise
	 * return false. It will wait till element is visible up to duration specified
	 * in seconds.
	 * 
	 * @param element - WebElement. WebElement for which want to wait.
	 * @param seconds - Integer. Till how many seconds to wait.
	 * @return - True if element displayed with in specified time otherwise False.
	 */
	public static Boolean isElementDisplayed(WebElement element, int seconds) {
		try {
			waitTillElementVisible(element, seconds);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Verify element is visible or not. If visible then return true otherwise
	 * return false. It will wait till element is visible up to duration specified
	 * in configuration file with property name 'explicitWait'.
	 * 
	 * @param element - WebElement. WebElement for which want to wait.
	 * @return - True if element displayed success otherwise False.
	 */
	public static Boolean isElementDisplayed(WebElement element) {
		return isElementDisplayed(element, Settings.explicitWait);
	}

	/**
	 * Verify element is present in DOM or not. If present then return true
	 * otherwise return false. It will wait till element is visible up to duration
	 * specified in seconds.
	 * 
	 * @param by      - By. Element using By mechanism.
	 * @param seconds - Integer. Till how many seconds to wait.
	 * @return - True if element present in DOM with in specified time otherwise
	 *         False.
	 */
	public static Boolean isElementPresentInDOM(By by, int seconds) {
		try {
			waitTillElementPresentInDOM(by, seconds);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Verify element is present in DOM or not. If present then return true
	 * otherwise return false. It will wait till element is visible up to duration
	 * specified in configuration file with property name 'explicitWait'.
	 * 
	 * @param by      - By. Element using By mechanism.
	 * @param seconds - Integer. Till how many seconds to wait.
	 * @return - True if element present in DOM with in specified time otherwise
	 *         False.
	 */
	public static Boolean isElementPresentInDOM(By by) {
		return isElementPresentInDOM(by, Settings.explicitWait);
	}

	/**
	 * Wait till element is clickable up to duration specified in seconds. If
	 * success then return true otherwise return false.
	 * 
	 * @param element - WebElement. WebElement for which want to wait.
	 * @param seconds - Integer. Till how many seconds to wait.
	 * @return - True if element clickable with in specified time otherwise False.
	 */
	public static Boolean isElementClickable(WebElement element, int seconds) {
		try {
			waitTillElementClickable(element, seconds);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Wait till element is clickable up to duration specified in configuration file
	 * with property name 'explicitWait'. If success then return true otherwise
	 * return false.
	 * 
	 * @param element - WebElement. WebElement for which want to click.
	 * @return - True if element clickable otherwise False.
	 */
	public static Boolean isElementClickable(WebElement element) {
		return isElementClickable(element, Settings.explicitWait);
	}

	// Mouse related actions on Element

	/**
	 * Hover to the element. It will wait till element is visible up to duration
	 * specified in configuration file with property name 'explicitWait'.
	 * 
	 * @param element - WebElement. WebElement for which want to hover.
	 */
	public static void hoverElement(WebElement element) {
		Actions action = new Actions(LocalWebDriverContext.getLocalDriver());

		waitTillElementVisible(element);
		action.moveToElement(element).build().perform();
	}

	/**
	 * Double click on the element. It will wait till element is visible up to
	 * duration specified in configuration file with property name 'explicitWait'.
	 * 
	 * @param element - WebElement. WebElement for which want to double click.
	 */
	public static void doubleClickOnElement(WebElement element) {
		Actions action = new Actions(LocalWebDriverContext.getLocalDriver());

		waitTillElementVisible(element);
		action.doubleClick(element).build().perform();
		log.info("Double clicked on element.");
	}

	/**
	 * Double click on the current position.
	 * 
	 */
	public static void doubleClick() {
		Actions action = new Actions(LocalWebDriverContext.getLocalDriver());
		action.doubleClick().build().perform();
		log.info("Double clicked on current position.");
	}

	/**
	 * Right click on the element. It will wait till element is visible up to
	 * duration specified in configuration file with property name 'explicitWait'.
	 * 
	 * @param element - WebElement. WebElement for which want to right click.
	 */
	public static void rightClickOnElement(WebElement element) {
		Actions action = new Actions(LocalWebDriverContext.getLocalDriver());

		waitTillElementVisible(element);
		action.contextClick(element).build().perform();
		log.info("Right clicked on element.");
	}

	/**
	 * Right click on the current position.
	 */
	public static void rightClick() {
		Actions action = new Actions(LocalWebDriverContext.getLocalDriver());
		action.contextClick().build().perform();
		log.info("Double clicked on current position.");
	}

	/**
	 * Drag and drop on the element at destination. It will wait till element is
	 * visible up to duration specified in configuration file with property name
	 * 'explicitWait'.
	 * 
	 * @param element     - WebElement. WebElement which want to drag and drop.
	 * @param destination - WebElement. Target destination WebElemetn where want to
	 *                    drop specified WebElement.
	 */
	public static void dragAndDropElement(WebElement element, WebElement destination) {
		Actions action = new Actions(LocalWebDriverContext.getLocalDriver());

		waitTillElementVisible(element);
		action.dragAndDrop(element, destination).build().perform();
		log.info("Drag and Drop element.");
	}

	/**
	 * Click and hold on the element. It will wait till element is visible up to
	 * duration specified in configuration file with property name 'explicitWait'.
	 * 
	 * @param element - WebElement. WebElement for which want to click and hold.
	 */
	public static void clickAndHoldOnElement(WebElement element) {
		Actions action = new Actions(LocalWebDriverContext.getLocalDriver());

		waitTillElementVisible(element);
		action.clickAndHold(element).build().perform();
		log.info("Clicked and hold on element.");
	}

	/**
	 * Click and hold on the current position.
	 */
	public static void clickAndHold() {
		Actions action = new Actions(LocalWebDriverContext.getLocalDriver());
		action.clickAndHold().build().perform();
		log.info("Clicked and hold on current position.");
	}

	/**
	 * Press key from keyboard on element. It will wait till element is visible up
	 * to duration specified in configuration file with property name
	 * 'explicitWait'.
	 * 
	 * @param element - WebElement. WebElement on which want to press key.
	 * @param k       - Keys. Key which want to press.
	 */
	public static void pressKeyOneTime(WebElement element, Keys k) {
		Actions action = new Actions(LocalWebDriverContext.getLocalDriver());

		waitTillElementVisible(element);
		action.keyDown(element, k).keyUp(element, k).build().perform();
		log.info("Key pressed on element.");
	}

	/**
	 * Press key from keyboard.
	 * 
	 * @param k - Keys. Key which want to press.
	 */
	public static void pressKeyOneTime(Keys k) {
		Actions action = new Actions(LocalWebDriverContext.getLocalDriver());
		action.keyDown(k).keyUp(k).build().perform();
		log.info("Key pressed on keyboard.");
	}

}
