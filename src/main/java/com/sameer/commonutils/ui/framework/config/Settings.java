package com.sameer.commonutils.ui.framework.config;

import com.sameer.commonutils.ui.framework.driver.BrowserType;

/**
 * This class is for store all the properties value into variable. So, it will
 * used anywhere throughout the execution. No need to get call every time from
 * Properties class.
 */

public class Settings {

	public static String OS;
	public static BrowserType browserType;
	public static Boolean remote;
	public static String appURL;
	public static String username;
	public static String password;
	public static int explicitWait;
	public static String timeStampFormat;

	public static String hubURL;

	// Extent report settings
	public static String documentName;
	public static String reportName;
	public static String timeStamp;
	public static String theme;
	public static String pathToSave;
	public static String htmlReportName;

	// Database settings
	public static String connectionString;

}
