package com.sameer.commonutils.ui.framework.common;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import com.sameer.commonutils.ui.framework.base.LocalWebDriverContext;

/**
 * This class contains method for taking screenshot.
 */
public class TakeScreenshot {
	private static final Logger log = LogManager.getLogger(TakeScreenshot.class);

	/**
	 * It takes screenshot, save the screenshot file and return file path.
	 * 
	 * @param fileName   - String. Name of file.
	 * @param pathToSave - String. Path where you want to save file.
	 * @return - Path of saved file.
	 */
	public static String take(String fileName, String pathToSave) {
		String path = GeneralUtils.normalizedFilePath(pathToSave) + fileName + ".png";
		try {
			TakesScreenshot screenShot = ((TakesScreenshot) LocalWebDriverContext.getLocalDriver());
			File srcFile = screenShot.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(srcFile, new File(path));
		} catch (Exception e) {
			log.error("Error in take screen shot", e);
		}

		return path;
	}

	/**
	 * It takes screenshot, save the screenshot file and return file.
	 * 
	 * @param fileName   - String. Name of file.
	 * @param pathToSave - String. Path where you want to save file.
	 * @return - If success then Save file otherwise return null.
	 */
	public static File take_(String fileName, String pathToSave) {
		String path = GeneralUtils.normalizedFilePath(pathToSave) + fileName + ".png";
		try {
			TakesScreenshot screenShot = ((TakesScreenshot) LocalWebDriverContext.getLocalDriver());
			File srcFile = screenShot.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(srcFile, new File(path));
			return srcFile;
		} catch (Exception e) {
			log.error("Error in take screen shot", e);
		}

		return null;
	}

}
