package com.sameer.commonutils.ui.framework.common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

/**
 * This class is assert functionality. It contains methods related to assertion
 * with pass/fail logging mechanism.
 */
public class Asserts {
	private static final Logger log = LogManager.getLogger(Asserts.class);

	/**
	 * Asserts that a condition is true. If it isn't, an AssertionError is thrown.
	 * 
	 * @param condition - Boolean. the condition to evaluate
	 */
	public static void assertTrue(Boolean condition) {
		assertTrue(condition, "Expected [True] found [False]");
	}

	/**
	 * Asserts that a condition is true. If it isn't, an AssertionError, with the
	 * given message, is thrown.
	 * 
	 * @param condition    - Boolean. the condition to evaluate
	 * @param errorMessage - String. the assertion error message
	 */
	public static void assertTrue(Boolean condition, String errorMessage) {

		try {
			Assert.assertTrue(condition, errorMessage);
			log.info("Verification point passed.");
			ReportUtils.addLog(Status.PASS, "Verification point passed.");

		} catch (AssertionError e) {
			log.error(errorMessage);
			ReportUtils.addLog(Status.FAIL, errorMessage);
			ReportUtils.addLog(Status.FAIL, new AssertionError(e));
			throw new AssertionError(e);
		}
	}

	/**
	 * Asserts that a condition is false. If it isn't, an AssertionError is thrown.
	 * 
	 * @param condition - Boolean. the condition to evaluate
	 */
	public static void assertFalse(Boolean condition) {
		assertFalse(condition, "Expected [False] found [True]");
	}

	/**
	 * Asserts that a condition is false. If it isn't, an AssertionError, with the
	 * given message, is thrown.
	 * 
	 * @param condition    - Boolean. the condition to evaluate
	 * @param errorMessage - String. the assertion error message
	 */
	public static void assertFalse(Boolean condition, String errorMessage) {

		try {
			Assert.assertFalse(condition, errorMessage);
			log.info("Verification point passed.");
			ReportUtils.addLog(Status.PASS, "Verification point passed.");
		} catch (AssertionError e) {
			log.error(errorMessage);
			ReportUtils.addLog(Status.FAIL, errorMessage);
			ReportUtils.addLog(Status.FAIL, new AssertionError(e));
			throw new AssertionError(e);
		}
	}

	/**
	 * Asserts that a object is null. If it isn't, an AssertionError, is thrown.
	 * 
	 * @param object - Object. object the assertion object
	 */
	public static void assertNull(Object object) {
		assertNull(object, "Expected [null] found [not null]");
	}

	/**
	 * Asserts that a object is null. If it isn't, an AssertionError, with the given
	 * message, is thrown.
	 * 
	 * @param object       - Object. object the assertion object
	 * @param errorMessage - String. the assertion error message
	 */
	public static void assertNull(Object object, String errorMessage) {

		try {
			Assert.assertNull(object, errorMessage);
			log.info("Verification point passed.");
			ReportUtils.addLog(Status.PASS, "Verification point passed.");

		} catch (AssertionError e) {
			log.error(errorMessage);
			ReportUtils.addLog(Status.FAIL, errorMessage);
			ReportUtils.addLog(Status.FAIL, new AssertionError(e));
			throw new AssertionError(e);
		}

	}

	/**
	 * Asserts that a object is not null. If it isn't, an AssertionError, is thrown.
	 * 
	 * @param object - Object. object the assertion object
	 */
	public static void assertNotNull(Object object) {
		assertNotNull(object, "Expected [not null] found [null]");
	}

	/**
	 * Asserts that a object is not null. If it isn't, an AssertionError, with the
	 * given message, is thrown.
	 * 
	 * @param object       - Object. object the assertion object
	 * @param errorMessage - String. the assertion error message
	 */
	public static void assertNotNull(Object object, String errorMessage) {
		try {
			Assert.assertNotNull(object, errorMessage);
			log.info("Verification point passed.");
			ReportUtils.addLog(Status.PASS, "Verification point passed.");

		} catch (AssertionError e) {
			log.error(errorMessage);
			ReportUtils.addLog(Status.FAIL, errorMessage);
			ReportUtils.addLog(Status.FAIL, new AssertionError(e));
			throw new AssertionError(e);
		}
	}

	/**
	 * Asserts that two objects are equal. If they are not, an AssertionError, with
	 * the given message, is thrown.
	 * 
	 * @param object1      - Object. the actual value
	 * @param object2      - Object. the expected value
	 * @param errorMessage - String. the assertion error message
	 */
	public static void assertEqual(Object object1, Object object2, String errorMessage) {

		try {
			Assert.assertEquals(object1, object2, errorMessage);
			log.info("Verification point passed.");
			ReportUtils.addLog(Status.PASS, "Verification point passed.");

		} catch (AssertionError e) {
			log.error(errorMessage);
			ReportUtils.addLog(Status.FAIL, errorMessage);
			ReportUtils.addLog(Status.FAIL, new AssertionError(e));
			throw new AssertionError(e);

		}
	}

	/**
	 * Asserts that two objects are equal. If they are not, an AssertionError, is
	 * thrown.
	 * 
	 * @param object1 - Object. the actual value
	 * @param object2 - Object. the expected value
	 */
	public static void assertEqual(Object object1, Object object2) {
		assertEqual(object1, object2, object1.toString() + " and " + object2.toString()
				+ " are not equal. It should be equal. Actual object : " + object1);
	}

	/**
	 * Asserts that two objects are not equal. If they are not, an AssertionError,
	 * with the given message, is thrown.
	 * 
	 * @param object1      - Object. the actual value
	 * @param object2      - Object. the expected value
	 * @param errorMessage - String. the assertion error message
	 */
	public static void assertNotEqual(Object object1, Object object2, String errorMessage) {
		try {
			Assert.assertNotEquals(object1, object2, errorMessage);
			log.info("Verification point passed.");
			ReportUtils.addLog(Status.PASS, "Verification point passed.");

		} catch (AssertionError e) {
			log.error(errorMessage);
			ReportUtils.addLog(Status.FAIL, errorMessage);
			ReportUtils.addLog(Status.FAIL, new AssertionError(e));
			throw new AssertionError(e);

		}
	}

	/**
	 * Asserts that two objects are not equal. If they are not, an AssertionError,
	 * is thrown.
	 * 
	 * @param object1 - Object. the actual value
	 * @param object2 - Object. the expected value
	 */
	public static void assertNotEqual(Object object1, Object object2) {
		assertNotEqual(object1, object2, object1.toString() + " and " + object2.toString()
				+ " are equal. It should be not equal. Actual object : " + object1);
	}

}
