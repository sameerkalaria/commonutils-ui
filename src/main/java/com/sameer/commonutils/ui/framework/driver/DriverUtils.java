package com.sameer.commonutils.ui.framework.driver;

import java.time.Duration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.Status;
import com.sameer.commonutils.ui.framework.base.LocalWebDriverContext;
import com.sameer.commonutils.ui.framework.common.ReportUtils;

/**
 * This class contains all methods related to driver context.
 */
public class DriverUtils {

	private static final Logger log = LogManager.getLogger(DriverUtils.class);

	/**
	 * Go to URL pass as parameter. It will wait for page load up to 30 seconds
	 * 
	 * @param url - String. URL where to navigate.
	 * @throws Exception
	 */
	public static void goToURL(String url) throws Exception {
		LocalWebDriverContext.getLocalDriver().get(url);
		waitTillPageLoad(30);
	}

	/**
	 * Maximize current browse context
	 */
	public static void maximize() {
		LocalWebDriverContext.getLocalDriver().manage().window().maximize();
		log.info("Browser maximized");
	}

	/**
	 * Wait till page is loaded up to duration pass in seconds
	 * 
	 * @param seconds - Integer. How much time to wait in seconds.
	 * @throws Exception
	 */
	public static void waitTillPageLoad(int seconds) throws Exception {
		WebDriverWait wait = new WebDriverWait(LocalWebDriverContext.getLocalDriver(), Duration.ofSeconds(seconds));

		try {
			wait.until(js -> ((JavascriptExecutor) LocalWebDriverContext.getLocalDriver())
					.executeScript("return document.readyState").toString().equals("complete"));

			log.info("Page is loaded.");
		} catch (Exception e) {
			log.debug("Page is not fully loaded within " + seconds + " seconds", e);
			ReportUtils.addLog(Status.FAIL, e);
			throw new Exception(e);
		}
	}

}
