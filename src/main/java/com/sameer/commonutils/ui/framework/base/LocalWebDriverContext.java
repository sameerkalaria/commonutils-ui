package com.sameer.commonutils.ui.framework.base;

import org.openqa.selenium.WebDriver;

/**
 * This class is for handle local WebDriver context. It uses ThreadLocal for
 * storing WebDriver context. So, all WebDriver context will be thread safe
 * during parallel execution.
 */
public class LocalWebDriverContext {
	private static ThreadLocal<WebDriver> webDriver = new ThreadLocal<>();

	/**
	 * Get Object of WebDriver from ThreadLocal. It will thread safe.
	 * 
	 * @return - Object of WebDriver.
	 */
	public static WebDriver getLocalDriver() {
		return webDriver.get();
	}

	/**
	 * Set object of WebDriver into ThreadLocal.
	 * 
	 * @param req - Object of WebDriver.
	 */
	public static void setLocalDriver(WebDriver driver) {
		webDriver.set(driver);
	}

	/**
	 * Close WebDriver instance and remove form ThreadLocal variable.
	 */
	public static void closeDriver() {
		webDriver.get().close();
		webDriver.remove();
	}

}
