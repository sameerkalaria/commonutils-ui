package com.sameer.commonutils.ui.framework.common;

import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.sameer.commonutils.ui.framework.config.Settings;

/**
 * This class contains all static methods for general purpose use like
 * timestamp, file path, images related etc.
 */
public class GeneralUtils {
	private static final Logger log = LogManager.getLogger(GeneralUtils.class);

	public static String getCurrentTimeStamp() {
		DateFormat df = new SimpleDateFormat(Settings.timeStampFormat);
		Date today = Calendar.getInstance().getTime();
		return df.format(today);
	}

	public static String getCurrentTimeStamp(String formatString) {
		DateFormat df = new SimpleDateFormat(formatString);
		Date today = Calendar.getInstance().getTime();
		return df.format(today);
	}

	public static String getCurrentUtcTime() {
		Instant instant = Instant.now();
		return instant.toString();
	}

	public static String getUtcTimeAfterHours(long hours) {
		Instant instant = Instant.now().plus(Duration.ofHours(hours));
		return instant.toString();
	}

	public static String getUtcTimeAfterDays(long days) {
		Instant instant = Instant.now().plus(Duration.ofDays(days));
		return instant.toString();
	}

	public static String getUtcTimeAfterMinutes(long minutes) {
		Instant instant = Instant.now().plus(Duration.ofMinutes(minutes));
		return instant.toString();
	}

	public static String getUtcTimeBeforeHours(long hours) {
		Instant instant = Instant.now().minus(Duration.ofHours(hours));
		return instant.toString();
	}

	public static String getUtcTimeBeforeMinutes(long minutes) {
		Instant instant = Instant.now().minus(Duration.ofMinutes(minutes));
		return instant.toString();
	}

	public static String getUtcTimeBeforeDays(long days) {
		Instant instant = Instant.now().minus(Duration.ofDays(days));
		return instant.toString();
	}

	public static void sleep(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
			log.error("Error while wait", e);
		}
	}

	public boolean compareImages(BufferedImage img1, BufferedImage img2) {
		boolean compare = true;
		try {
			if (img1.getWidth() == img2.getWidth() && img1.getHeight() == img2.getHeight()) {
				for (int i = 0; i < img1.getWidth(); i++) {
					for (int j = 0; j < img1.getHeight(); j++) {
						if (img1.getRGB(i, j) != img2.getRGB(i, j)) {
							compare = false;
						}
					}
				}
			} else {
				compare = false;
			}
		} catch (Exception E) {
			compare = false;
		}

		return compare;
	}

	public static Document convertPdfToHTML() {
		try {
			Document document = new Document();

			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("D://test.pdf"));

			document.open();

			XMLWorkerHelper.getInstance().parseXHtml(writer, document, new FileInputStream(
					"D://Study//Selenium//TestNG_Workspace//TestNGProject//test-output//emailable-report.html"));

			document.close();

			log.info("PDF Created!");

			return document;
		} catch (Exception e) {
			log.error("Error in PDF creation.", e);
		}

		return null;
	}

	// This method will add current directory at starting to your path
	public static String normalizedFilePath(String path) {
		path = path.endsWith("/") ? path : path + "/";
		String normalizedPath = System.getProperty("user.dir") + path;
		return normalizedPath;
	}

}
