package com.sameer.commonutils.ui.framework.base;

import org.openqa.selenium.support.PageFactory;

/**
 * This is Base class for BasePage and BaseTest class. It contains method
 * getInstance for getting page instance of any page type.
 */
public class Base {
	protected BasePage currentPage;

	/**
	 * It will initialize all Page objects of specified Page class and return object
	 * of that page class.
	 * 
	 * @param <TPage> - Generic Class
	 * @param page    - Class<TPage>. Any class which is child of BasePage class.
	 * @return - Object of specified Page class with initialize all Page objects.
	 */
	public <TPage extends BasePage> TPage getInstance(Class<TPage> page) {
		// Custom control page factory initialization
		Object obj = PageFactory.initElements(LocalWebDriverContext.getLocalDriver(), page);

		return page.cast(obj);
	}

}
