package com.sameer.commonutils.ui.framework.common;

import com.aventstack.extentreports.ExtentTest;

/**
 * This class is for handle Extent report context. It is thread safe while
 * running parallel execution.
 */
public class LocalExtentTestContext {
	private static ThreadLocal<ExtentTest> test = new ThreadLocal<>();

	/**
	 * Get Object of ExtentTest from ThreadLocal. It will thread safe.
	 * 
	 * @return - Object of ExtentTest.
	 */
	public static ExtentTest getExtentTest() {
		return test.get();
	}

	/**
	 * Set object of ExtentTest into ThreadLocal.
	 * 
	 * @param t - Object of ExtentTest.
	 */
	public static void setExtentTest(ExtentTest t) {
		test.set(t);
	}

}
