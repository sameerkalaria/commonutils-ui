package com.sameer.commonutils.ui.framework.common;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * This class contain method related to write data in text file.
 */
public class WriteData {

	private static final Logger log = LogManager.getLogger(WriteData.class);

	public static void toTxtFile(String filePath, String[][] data, Boolean append) {
		try {
			File file = new File(filePath);

			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsolutePath(), append);
			BufferedWriter bfw = new BufferedWriter(fw);

			bfw.newLine();

			for (int i = 0; i < data.length; i++) {
				bfw.newLine();
				for (int j = 0; j < data[0].length; j++) {
					bfw.write(data[i][j]);
					bfw.write(" : ");
				}
			}
			bfw.close();
		} catch (Exception e) {
			log.error("Error in write data into text file.", e);
		}
	}

	public static void toTxtFile(String filePath, String data, Boolean append) {
		try {
			File file = new File(filePath);

			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsolutePath(), append);
			BufferedWriter bfw = new BufferedWriter(fw);

			bfw.newLine();
			bfw.write(data);

			bfw.close();
		} catch (Exception e) {
			log.error("Error in write data into text file.", e);
		}
	}

	public static void createExcel(String filePath, String sheetName) {
		try {

			File file = new File(filePath);

			if (file.exists()) {
				file.delete();
			}

			file.createNewFile();
			FileOutputStream fileOutputStream = new FileOutputStream(file);
			XSSFWorkbook workbook = new XSSFWorkbook();
			workbook.createSheet(sheetName);
			workbook.write(fileOutputStream);
			workbook.close();
			fileOutputStream.close();

		} catch (FileNotFoundException e) {
			log.error(filePath + " file is not found.", e);
		} catch (Exception e) {
			log.error("Error in writing data into excel file.", e);
		}
	}

	public static void toExcel(String filePath, String sheetName, String[] data) {

		try {
			FileInputStream fileInputStream = new FileInputStream(filePath);

			XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);
			XSSFSheet sheet = workbook.getSheet(sheetName);

			int rowNumber = sheet.getLastRowNum() + 1;
			Row row = sheet.createRow(rowNumber);
			for (int i = 0; i < data.length; i++) {
				Cell cell = row.createCell(i);
				cell.setCellValue(data[i]);

			}

			FileOutputStream fileOutputStream = new FileOutputStream(filePath);
			workbook.write(fileOutputStream);
			workbook.close();
			fileOutputStream.close();

		} catch (FileNotFoundException e) {
			log.error(filePath + " file is not found.", e);
		} catch (Exception e) {
			log.error("Error in writing data into excel file.", e);
		}

	}

	public static void toExcel(String filePath, String sheetName, String[][] data) {

		try {
			FileInputStream fileInputStream = new FileInputStream(filePath);

			XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);
			XSSFSheet sheet = workbook.getSheet(sheetName);

			int rowNumber = sheet.getLastRowNum();

			for (int i = 0; i < data.length; i++) {
				Row row = sheet.createRow(++rowNumber);

				for (int j = 0; j < data[0].length; j++) {
					Cell cell = row.createCell(j);
					cell.setCellValue(data[i][j]);

				}
			}

			FileOutputStream fileOutputStream = new FileOutputStream(filePath);
			workbook.write(fileOutputStream);
			workbook.close();
			fileOutputStream.close();

		} catch (FileNotFoundException e) {
			log.error(filePath + " file is not found.", e);
		} catch (Exception e) {
			log.error("Error in writing data into excel file.", e);
		}

	}

	public static void toExcelHeader(String filePath, String sheetName, String[] data) {

		try {
			FileInputStream fileInputStream = new FileInputStream(filePath);

			XSSFWorkbook workbook = new XSSFWorkbook(fileInputStream);
			XSSFSheet sheet = workbook.getSheet(sheetName);

			// For setting cell style
			CellStyle cellStyle = workbook.createCellStyle();
			cellStyle.setFillForegroundColor(IndexedColors.BLUE_GREY.getIndex());
			cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			cellStyle.setAlignment(HorizontalAlignment.CENTER);
			cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);

			Font font = workbook.createFont();
			font.setColor(IndexedColors.WHITE.getIndex());
			font.setBold(true);
			font.setFontHeightInPoints((short) 12);
			cellStyle.setFont(font);

			int rowNumber = sheet.getLastRowNum() + 1;
			Row row = sheet.createRow(rowNumber);
			row.setHeightInPoints(25);

			for (int i = 0; i < data.length; i++) {
				Cell cell = row.createCell(i);
				cell.setCellValue(data[i]);

				cell.setCellStyle(cellStyle);

			}

			FileOutputStream fileOutputStream = new FileOutputStream(filePath);
			workbook.write(fileOutputStream);
			workbook.close();
			fileOutputStream.close();

		} catch (FileNotFoundException e) {
			log.error(filePath + " file is not found.", e);
		} catch (Exception e) {
			log.error("Error in writing data into excel file.", e);
		}

	}

}
