package com.sameer.commonutils.ui.framework.driver;

/**
 * Enum for Browser types
 */
public enum BrowserType {
	CHROME, FIREFOX, SAFARI
}
